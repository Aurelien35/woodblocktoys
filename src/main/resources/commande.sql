--DROP TABLE if exists client;
CREATE TABLE client
(
id_client varchar (40) primary key,
nom varchar (40) not null,
prenom varchar (40) not null,
telephone integer,
email varchar (40) not null,
mdp varchar (40) not null,
numero_de_rue integer,
nom_de_rue varchar (40),
complement_adresse varchar (40),
code_postal integer,
ville varchar (40)
);
insert into client (id_client, nom, prenom, telephone, email, mdp, numero_de_rue, nom_de_rue, complement_adresse, code_postal, ville) values ('4d383fac-0941-4f36-967a-fedbf07fefcb', 'Dupont', 'François', 0699999999, 'françois-dupond@monadresse.com', 'mysecretpassword', 5, 'Rue des Accacias', '', 75000, 'Paris')


CREATE TABLE commande
(
id_commande varchar (40) primary key,
prix integer not null,
nb_pieces integer not null,
id_client varchar (40),
etat varchar (40) not null
);
insert into commande (id_commande, prix, nb_pieces, id_client, etat) values ('4d383fac-0941-4f3a-967a-fedbf07fefcb', 70, 45, '4d383fac-0941-4f36-967a-fedbf07fefcb', 'en cours de preparation')

CREATE TABLE bois
(
essence varchar (40) primary key,
prix_du_bois real
);

CREATE TABLE peinture
(
finition varchar (40) primary key,
prix_peinture real
);