package com.woodblocktoys.woodblocktoys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoodblocktoysApplication {

	public static void main(String[] args) {
		SpringApplication.run(WoodblocktoysApplication.class, args);
	}

}
