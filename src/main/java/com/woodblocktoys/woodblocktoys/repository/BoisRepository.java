package com.woodblocktoys.woodblocktoys.repository;

import com.woodblocktoys.woodblocktoys.domain.Bois;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoisRepository extends CrudRepository <Bois, String> {
}
