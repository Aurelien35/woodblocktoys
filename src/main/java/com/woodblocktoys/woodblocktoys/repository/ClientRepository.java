package com.woodblocktoys.woodblocktoys.repository;

import com.woodblocktoys.woodblocktoys.domain.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository <Client, String> {
}
