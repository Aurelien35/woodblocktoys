package com.woodblocktoys.woodblocktoys.repository;

import com.woodblocktoys.woodblocktoys.domain.Peinture;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PeintureRepository extends CrudRepository <Peinture, String> {
}
