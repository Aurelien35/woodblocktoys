package com.woodblocktoys.woodblocktoys.repository;


import com.woodblocktoys.woodblocktoys.domain.Commande;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommandeRepository extends CrudRepository <Commande,String> {
}
