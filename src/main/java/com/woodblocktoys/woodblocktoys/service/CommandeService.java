package com.woodblocktoys.woodblocktoys.service;


import com.woodblocktoys.woodblocktoys.controller.representation.NewCommande;
import com.woodblocktoys.woodblocktoys.domain.Commande;
import com.woodblocktoys.woodblocktoys.factory.IdClientGenerator;
import com.woodblocktoys.woodblocktoys.repository.CommandeRepository;
import com.woodblocktoys.woodblocktoys.domain.Commande;
import org.hibernate.mapping.IdGenerator;
import org.springframework.stereotype.Component;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Component
public class CommandeService {

    private CommandeRepository commandeRepository;
    private IdClientGenerator idClientGenerator;

    public CommandeService (CommandeRepository commandeRepository, IdClientGenerator idClientGenerator) {
        this.commandeRepository = commandeRepository;
        this.idClientGenerator = idClientGenerator;
    }

    public Optional<Commande>getOne(String id){
        return this.commandeRepository.findById(id);
    }

    public List<Commande>getAll(){
        return (List<Commande>) this.commandeRepository.findAll();
    }

    public void create (NewCommande newCommande){
        Commande commande = new Commande(idClientGenerator.generateNewIdCommande(),newCommande.getPrix(), newCommande.getNbPieces(), newCommande.getIdClient(), newCommande.getEtat());
        this.commandeRepository.save(commande);
    }

    public void modifieCommande (String idCommande, NewCommande body){
        Commande commandeToModify = this.commandeRepository.findById(idCommande).get();
        Commande commandeUser = new Commande (idCommande, body.getPrix(), body.getNbPieces(), body.getIdClient(), body.getEtat());
        double newPrix = body.getPrix();
        double newNbPieces = body.getNbPieces();
        String newIdClient = body.getIdClient();
        String newEtat = body.getEtat();

        Commande commandefinal = new Commande (idCommande, newPrix, newNbPieces,newIdClient, newEtat);
        this.commandeRepository.save(commandefinal);
    }
}
