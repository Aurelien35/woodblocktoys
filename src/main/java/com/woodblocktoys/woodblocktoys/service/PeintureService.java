package com.woodblocktoys.woodblocktoys.service;


import com.woodblocktoys.woodblocktoys.controller.representation.NewPeinture;
import com.woodblocktoys.woodblocktoys.domain.Peinture;
import com.woodblocktoys.woodblocktoys.repository.PeintureRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class PeintureService {

    private PeintureRepository peintureRepository;

    public PeintureService (PeintureRepository peintureRepository){
        this.peintureRepository = peintureRepository;
    }

    public Optional<Peinture> getOne (String finition){
        return this.peintureRepository.findById(finition);
    }

    public List<Peinture> getAll () {
        return (List<Peinture>) this.peintureRepository.findAll();
    }

    public void create (NewPeinture newPeinture){
        Peinture peinture = new Peinture (newPeinture.getFinition(), newPeinture.getPrixPeinture());
        this.peintureRepository.save(peinture);
    }

    public void modifiePeinture (String finition, NewPeinture body){
        Peinture peintureToModify = this.peintureRepository.findById(finition).get();
        Peinture peintureUser = new Peinture(body.getFinition(), body.getPrixPeinture());
        String newFinition = peintureToModify.getFinition();
        double newPrixPeinture = peintureToModify.getPrixPeinture();

        Peinture peintureFinal = new Peinture(newFinition, newPrixPeinture);

        this.peintureRepository.save(peintureFinal);

    }
}
