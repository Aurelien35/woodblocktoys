package com.woodblocktoys.woodblocktoys.service;


import com.woodblocktoys.woodblocktoys.controller.representation.NewBois;
import com.woodblocktoys.woodblocktoys.domain.Bois;
import com.woodblocktoys.woodblocktoys.repository.BoisRepository;
import org.aspectj.bridge.ICommand;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BoisService {

    private BoisRepository boisRepository;

    public BoisService (BoisRepository boisRepository){
        this.boisRepository = boisRepository;
    }

    public Optional<Bois> getOne(String essence) {
        return this.boisRepository.findById(essence);
    }

    public List<Bois> getAll() {
        return (List<Bois>) this.boisRepository.findAll();
    }

    public void create (NewBois newBois){
        Bois bois = new Bois (newBois.getEssence(), newBois.getPrixDuBois());
        this.boisRepository.save(bois);
    }

    public void modifieBois (String essence, NewBois body){
        Bois boisToModify = this.boisRepository.findById(essence).get();
        Bois boisUser = new Bois (essence, body.getPrixDuBois());
        double newPrixDuBois = body.getPrixDuBois();

        Bois boisfinal = new Bois(essence, newPrixDuBois);
        this.boisRepository.save(boisfinal);
    }


}
