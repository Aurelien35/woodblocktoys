package com.woodblocktoys.woodblocktoys.service;


import com.woodblocktoys.woodblocktoys.domain.Peinture;
import com.woodblocktoys.woodblocktoys.factory.IdClientGenerator;
import com.woodblocktoys.woodblocktoys.repository.BoisRepository;
import com.woodblocktoys.woodblocktoys.repository.ClientRepository;
import com.woodblocktoys.woodblocktoys.repository.CommandeRepository;
import com.woodblocktoys.woodblocktoys.repository.PeintureRepository;
import org.springframework.stereotype.Component;

@Component
public class CommandeGeneratorService {

   private CommandeRepository commandeRepository;
   private BoisRepository boisRepository;
   private PeintureRepository peintureRepository;
   private ClientRepository clientRepository;

   public CommandeGeneratorService (CommandeRepository commandeRepository, BoisRepository boisRepository, PeintureRepository peintureRepository, ClientRepository clientRepository){
      this.commandeRepository = commandeRepository;
      this.boisRepository = boisRepository;
      this.peintureRepository = peintureRepository;
      this.clientRepository = clientRepository;
   }

}
