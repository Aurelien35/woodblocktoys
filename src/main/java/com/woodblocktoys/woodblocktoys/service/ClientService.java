package com.woodblocktoys.woodblocktoys.service;

import com.woodblocktoys.woodblocktoys.domain.Client;
import com.woodblocktoys.woodblocktoys.controller.representation.NewClient;
import com.woodblocktoys.woodblocktoys.factory.IdClientGenerator;
import com.woodblocktoys.woodblocktoys.repository.ClientRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;


@Component
public class ClientService {

    private ClientRepository clientRepository;
    private IdClientGenerator idClientGenerator;


    public ClientService (ClientRepository clientRepository, IdClientGenerator idClientGenerator){
        this.clientRepository = clientRepository;
        this.idClientGenerator = idClientGenerator;
    }

    public Optional<Client> getOne(String id){
        return this.clientRepository.findById(id);
    }

    public List<Client> getAll(){
        return (List<Client>)this.clientRepository.findAll();
    }

    public void create (NewClient newClient){

        Client client = new Client(idClientGenerator.generateNewIdClient(), newClient.getNom(), newClient.getPrenom(), newClient.getEmail(),  newClient.getMdp(),newClient.getNumeroDeRue(), newClient.getTelephone(), newClient.getNomDeRue(), newClient.getComplementAdresse(), newClient.getCodePostal(), newClient.getVille());
        this.clientRepository.save(client);
    }

    public void modifieClient (String idClient, NewClient body){
        Client clientToModify = this.clientRepository.findById(idClient).get();
        Client clientUser = new Client (idClient, body.getNom(), body.getPrenom(), body.getEmail(),body.getMdp(),body.getTelephone(), body.getNumeroDeRue(), body.getNomDeRue(), body.getComplementAdresse(), body.getCodePostal(), body.getVille());
        String newNom = clientToModify.getNom();
        String newPrenom = clientToModify.getPrenom();
        String newEmail = clientToModify.getEmail();
        String newMdp = clientToModify.getMdp();
        int newTelephone = clientToModify.getTelephone();
        int newNumeroDeRue = clientToModify.getNumeroDeRue();
        String newNomDeRue = clientToModify.getNomDeRue();
        String newComplementAdresse = clientToModify.getComplementAdresse();
        int newCodePostal = clientToModify.getCodePostal();
        String newVille = clientToModify.getVille();

       Client clientFinal = new Client(idClient, newNom, newPrenom, newEmail, newMdp, newTelephone, newNumeroDeRue, newNomDeRue, newComplementAdresse, newCodePostal, newVille);

       this.clientRepository.save(clientFinal);

    }

}
