package com.woodblocktoys.woodblocktoys.factory;


import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class IdClientGenerator {

    private final UUID counter = null;

    public String generateNewIdClient(){
        return this.counter.randomUUID().toString();
    }


    public String generateNewIdCommande(){
        return this.counter.randomUUID().toString();
    }


}
