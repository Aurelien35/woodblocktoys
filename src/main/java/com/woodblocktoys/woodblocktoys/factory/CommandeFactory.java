package com.woodblocktoys.woodblocktoys.factory;


import org.springframework.stereotype.Component;

@Component
public class CommandeFactory {

    public String forme;
    public int volumeCarre;
    public int surfaceCarre;
    public int volumeTriangle;
    public int surfaceTriangle;
    public int volumeHexagone;
    public int surfaceHexagone;
    public int coutPeinture;
    public int coutBois;
    public int coutPiecePeinte;
    public int coutPieceTaille;
    public int c;
    public int h;
    public double surface;
    public double volume;

    public int getVolumeCarre() {
        volume = c*c*h;
        return volumeCarre;
    }

    public int getSurfaceCarre() {
        surface = (c*h)*4+(c*c)*2;
        return surfaceCarre;
    }

    public int getVolumeTriangle() {
        volume = Math.sqrt(3)*(Math.pow(c, 2)/4)*h;
        return volumeTriangle;
    }

    public int getSurfaceTriangle() {
        surface = (Math.sqrt(3)*(Math.pow(c, 2)/4)*h)+((c*h)*3);
        return surfaceTriangle;
    }

    public int getVolumeHexagone() {
        volume = (3*(Math.sqrt(3/2))*Math.pow(c, 2)*h);
        return volumeHexagone;
    }

    public int getSurfaceHexagone() {
        surface = (3 * (Math.sqrt(3 / 2)) * 2) + ((c * h) * 6);
        return surfaceHexagone;
    }

    public int getCoutPeinture() {
        if (forme == "carre") {
            coutPiecePeinte = surfaceCarre * coutPeinture;
        } else if (forme == "triangle") {
            coutPiecePeinte = surfaceTriangle * coutPeinture;
        } else if (forme == "hexagone") {
            coutPiecePeinte = surfaceHexagone * coutPeinture;
        }
        return coutPeinture;
    }


    public int getCoutBois() {
        if (forme == "carre"){
            coutPieceTaille = surfaceCarre*coutBois;
        }
        else if (forme == "triangle"){
            coutPieceTaille = surfaceTriangle*coutBois;
        }
        else if (forme == "hexagone"){
            coutPieceTaille = surfaceHexagone*coutBois;
        }
        return coutBois;
    }
}
