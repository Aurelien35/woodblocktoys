package com.woodblocktoys.woodblocktoys.controller;

import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableClientRepresentation;
import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableCommandeRepresentation;
import com.woodblocktoys.woodblocktoys.domain.Commande;
import org.springframework.stereotype.Component;

@Component
public class CommandeRepresentationMapper {

    public DisplayableCommandeRepresentation mapToDisplayableCommande (Commande commande){

        DisplayableCommandeRepresentation result = new DisplayableCommandeRepresentation();
        result.setIdCommande(commande.getIdCommande());
        result.setPrix(commande.getPrix());
        result.setNbPieces(commande.getNbPieces());
        result.setIdClient(commande.getIdClient());
        result.setEtat(commande.getEtat());

        return result;
    }
}
