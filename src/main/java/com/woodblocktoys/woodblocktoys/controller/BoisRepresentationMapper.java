package com.woodblocktoys.woodblocktoys.controller;


import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableBoisRepresentation;
import com.woodblocktoys.woodblocktoys.domain.Bois;
import org.springframework.stereotype.Component;

@Component
public class BoisRepresentationMapper {

  public DisplayableBoisRepresentation mapToDisplayableBois (Bois bois){

      DisplayableBoisRepresentation result = new DisplayableBoisRepresentation();
      result.setEssence(bois.getEssence());
      result.setPrixDuBois(bois.getPrixDuBois());

      return result;
  }
}
