package com.woodblocktoys.woodblocktoys.controller.representation;

public class NewPeinture {

    private String finition;
    private double prixPeinture;

    public String getFinition() {
        return finition;
    }

    public void setFinition(String finition) {
        this.finition = finition;
    }

    public double getPrixPeinture() {
        return prixPeinture;
    }

    public void setPrixPeinture(double prixPeinture) {
        this.prixPeinture = prixPeinture;
    }
}
