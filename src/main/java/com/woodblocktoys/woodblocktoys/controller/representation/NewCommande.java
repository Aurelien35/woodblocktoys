package com.woodblocktoys.woodblocktoys.controller.representation;

public class NewCommande {


    private double prix;
    private double nbPieces;
    private String idClient;
    private String etat;



    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public double getNbPieces() {
        return nbPieces;
    }

    public void setNbPieces(double nbPieces) {
        this.nbPieces = nbPieces;
    }

    public String getIdClient(){
        return idClient;
    }

    public void setIdClient(String idClient){
        this.idClient = idClient;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat){
        this.etat = etat;
    }
}
