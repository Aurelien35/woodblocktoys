package com.woodblocktoys.woodblocktoys.controller.representation;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude
public class DisplayableBoisRepresentation {

    public String essence;
    public double prixDuBois;



    public String getEssence() {
        return essence;
    }

    public void setEssence(String essence) {
        this.essence = essence;
    }

    public double getPrixDuBois() {
        return prixDuBois;
    }

    public void setPrixDuBois(double prixDuBois) {
        this.prixDuBois = prixDuBois;
    }
}
