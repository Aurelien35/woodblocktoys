package com.woodblocktoys.woodblocktoys.controller.representation;

public class NewBois {

    private String essence;
    private double prixDuBois;



    public String getEssence() {
        return essence;
    }

    public void setEssence(String essence) {
        this.essence = essence;
    }

    public double getPrixDuBois() {
        return prixDuBois;
    }

    public void setPrixDuBois(double prixDuBois) {
        this.prixDuBois = prixDuBois;
    }
}
