package com.woodblocktoys.woodblocktoys.controller;

import com.woodblocktoys.woodblocktoys.controller.representation.DisplayablePeintureRepresentation;
import com.woodblocktoys.woodblocktoys.domain.Peinture;
import org.springframework.stereotype.Component;


@Component
public class PeintureRepresentationMapper {

    public DisplayablePeintureRepresentation mapToDisplayablePeinture (Peinture peinture){

        DisplayablePeintureRepresentation result = new DisplayablePeintureRepresentation();
        result.setFinition(peinture.getFinition());
        result.setPrixPeinture(peinture.getPrixPeinture());

        return result;

    }
}
