package com.woodblocktoys.woodblocktoys.controller;

import com.woodblocktoys.woodblocktoys.domain.Client;
import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableClientRepresentation;
import org.springframework.stereotype.Component;


@Component
public class ClientRepresentationMapper {

    public DisplayableClientRepresentation mapToDisplayableClient (Client client){

        DisplayableClientRepresentation result = new DisplayableClientRepresentation();
        result.setIdClient(client.getIdClient());
        result.setNom(client.getNom());
        result.setPrenom(client.getPrenom());
        result.setEmail(client.getEmail());
        result.setMdp(client.getMdp());
        result.setTelephone(client.getTelephone());
        result.setNumeroDeRue(client.getNumeroDeRue());
        result.setNomDeRue(client.getNomDeRue());
        result.setComplementAdresse(client.getComplementAdresse());
        result.setCodePostal(client.getCodePostal());
        result.setVille(client.getVille());


        return result;

    }
}
