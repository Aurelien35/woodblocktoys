package com.woodblocktoys.woodblocktoys.controller;


import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableBoisRepresentation;
import com.woodblocktoys.woodblocktoys.controller.representation.NewBois;
import com.woodblocktoys.woodblocktoys.domain.Bois;
import com.woodblocktoys.woodblocktoys.service.BoisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/bois")

public class BoisController {

    private BoisService boisService;
    private BoisRepresentationMapper boisRepresentationMapper;

    @Autowired

    public BoisController (BoisService boisService, BoisRepresentationMapper boisRepresentationMapper) {
        this.boisService = boisService;
        this.boisRepresentationMapper = boisRepresentationMapper;
    }


    @GetMapping ("/{essence}")
    ResponseEntity<DisplayableBoisRepresentation>getOneEssence (@PathVariable ("essence") String essence){
        Optional<Bois>boisOptional = boisService.getOne(essence);

        return boisOptional
                .map(this.boisRepresentationMapper::mapToDisplayableBois)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
        List<DisplayableBoisRepresentation>getAllBois(){
            return this.boisService.getAll().stream()
                    .map(this.boisRepresentationMapper::mapToDisplayableBois)
                    .collect(Collectors.toList());
    }


    @PostMapping
        void create(@RequestBody NewBois newBois){
        boisService.create(newBois);
    }

    @PutMapping ("/{essence}")
    public ResponseEntity<DisplayableBoisRepresentation>modifieBois
            (@PathVariable("essence")String essence, @RequestBody NewBois body){
        this.boisService.modifieBois(essence, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
