package com.woodblocktoys.woodblocktoys.controller;

import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableCommandeRepresentation;
import com.woodblocktoys.woodblocktoys.controller.representation.NewCommande;
import com.woodblocktoys.woodblocktoys.domain.Commande;
import com.woodblocktoys.woodblocktoys.service.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/commande")
public class CommandeController {

    private CommandeService commandeService;
    private CommandeRepresentationMapper commandeRepresentationMapper;

    @Autowired

    public CommandeController (CommandeService commandeService, CommandeRepresentationMapper commandeRepresentationMapper) {
        this.commandeService = commandeService;
        this.commandeRepresentationMapper = commandeRepresentationMapper;
    }

    @GetMapping ("/{id}")

    ResponseEntity<DisplayableCommandeRepresentation>getOneCommande (@PathVariable ("id") String id){
        Optional<Commande> commandeOptional=commandeService.getOne(id);
        return commandeOptional
                .map(this.commandeRepresentationMapper::mapToDisplayableCommande)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    List<DisplayableCommandeRepresentation> getAllCommande(){
        return this.commandeService.getAll().stream()
                .map(this.commandeRepresentationMapper::mapToDisplayableCommande)
                .collect(Collectors.toList());
    }

    @PostMapping

        void create (@RequestBody NewCommande newCommande){
        commandeService.create(newCommande);
    }

    @PutMapping ("/{id}")

    public ResponseEntity<DisplayableCommandeRepresentation>modifieCommande (@PathVariable ("id") String id, @RequestBody NewCommande body){
        this.commandeService.modifieCommande(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}


