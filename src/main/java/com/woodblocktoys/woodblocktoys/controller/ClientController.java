package com.woodblocktoys.woodblocktoys.controller;

import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableClientRepresentation;
import com.woodblocktoys.woodblocktoys.controller.representation.NewClient;
import com.woodblocktoys.woodblocktoys.domain.Client;
import com.woodblocktoys.woodblocktoys.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/client")

public class ClientController {

    private ClientService clientService;
    private ClientRepresentationMapper clientRepresentationMapper;

@Autowired

    public ClientController (ClientService clientService, ClientRepresentationMapper clientRepresentationMapper){
        this.clientService = clientService;
        this.clientRepresentationMapper = clientRepresentationMapper;
}

@GetMapping ("/{id}")
    ResponseEntity<DisplayableClientRepresentation> getOneClient(@PathVariable ("id") String id){
    Optional<Client>clientOptional = clientService.getOne(id);

    return clientOptional
            .map(this.clientRepresentationMapper::mapToDisplayableClient)
            .map (ResponseEntity::ok)
            .orElse(ResponseEntity.notFound().build());
}

@GetMapping
    List<DisplayableClientRepresentation>getAllClient(){
    return this.clientService.getAll().stream()
            .map(this.clientRepresentationMapper::mapToDisplayableClient)
            .collect(Collectors.toList());
}

@PostMapping
    void create(@RequestBody NewClient newClient){
    clientService.create(newClient);
}

@PutMapping ("/{id}")
    public ResponseEntity<DisplayableClientRepresentation>modifieClient
        (@PathVariable("id") String id, @RequestBody NewClient body){
        this.clientService.modifieClient(id, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
}
}
