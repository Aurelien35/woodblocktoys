package com.woodblocktoys.woodblocktoys.controller;


import com.woodblocktoys.woodblocktoys.controller.representation.DisplayableClientRepresentation;
import com.woodblocktoys.woodblocktoys.controller.representation.DisplayablePeintureRepresentation;
import com.woodblocktoys.woodblocktoys.controller.representation.NewClient;
import com.woodblocktoys.woodblocktoys.controller.representation.NewPeinture;
import com.woodblocktoys.woodblocktoys.domain.Client;
import com.woodblocktoys.woodblocktoys.domain.Peinture;
import com.woodblocktoys.woodblocktoys.service.PeintureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping ("/peinture")

public class PeintureController {

    private PeintureService peintureService;
    private PeintureRepresentationMapper peintureRepresentationMapper;

    @Autowired

    public PeintureController(PeintureService peintureService, PeintureRepresentationMapper peintureRepresentationMapper) {
        this.peintureService = peintureService;
        this.peintureRepresentationMapper = peintureRepresentationMapper;
    }

    @GetMapping("/{finition}")
    ResponseEntity<DisplayablePeintureRepresentation> getOnePeinture(@PathVariable("peinture") String finition) {
        Optional<Peinture> peintureOptional = peintureService.getOne(finition);

        return peintureOptional
                .map(this.peintureRepresentationMapper::mapToDisplayablePeinture)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping
    List<DisplayablePeintureRepresentation> getAllPeinture() {
        return this.peintureService.getAll().stream()
                .map(this.peintureRepresentationMapper::mapToDisplayablePeinture)
                .collect(Collectors.toList());
    }

    @PostMapping
    void create(@RequestBody NewPeinture newPeinture){
        peintureService.create(newPeinture);
    }

    @PutMapping ("/{finition}")
    public ResponseEntity<DisplayablePeintureRepresentation>modifiePeinture
            (@PathVariable("finition") String finition, @RequestBody NewPeinture body){
        this.peintureService.modifiePeinture(finition, body);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
