package com.woodblocktoys.woodblocktoys.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name= "peinture")
public class Peinture {

    @Id

    private String finition;
    private double prixPeinture;


    protected Peinture () {}

    public Peinture (String finition, double prixPeinture){
        this.finition = finition;
        this.prixPeinture = prixPeinture;
    }

    public String getFinition() {
        return finition;
    }

    public void setFinition(String finition) {
        this.finition = finition;
    }

    public double getPrixPeinture() {
        return prixPeinture;
    }

    public void setPrixPeinture(double prixPeinture) {
        this.prixPeinture = prixPeinture;
    }
}
