package com.woodblocktoys.woodblocktoys.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table (name = "bois")
public class Bois {

    @Id

    private String essence;
    private double prixDuBois;

    protected Bois (){}

    public Bois (String essence, double prixDuBois){
        this.essence = essence;
        this.prixDuBois = prixDuBois;
    }

    public String getEssence() {
        return essence;
    }

    public void setEssence(String essence) {
        this.essence = essence;
    }

    public double getPrixDuBois() {
        return prixDuBois;
    }

    public void setPrixDuBois(double prixDuBois) {
        this.prixDuBois = prixDuBois;
    }
}
