package com.woodblocktoys.woodblocktoys.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "commande")
public class Commande {

    @Id

    private  String idCommande;
    private double prix;
    private double nbPieces;
    private String idClient;
    private String etat;

    protected Commande (){}


    public Commande (String idCommande,double prix, double nbPieces, String idClient, String etat){
        this.idCommande = idCommande;
        this.prix = prix;
        this.nbPieces = nbPieces;
        this.idClient = idClient;
        this.etat = etat;
    }

    public String getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(String idCommande) {
        this.idCommande = idCommande;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public double getNbPieces() {
        return nbPieces;
    }

    public void setNbPieces(double nbPieces) {
        this.nbPieces = nbPieces;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat){
        this.etat = etat;
    }
}
